## This is a file for translators.

## The translators are:
<!--Please add your name (and if you want add your public email. Never give out your personal email on the internet.)-->

## Some notes for translators:
Any letter with a random underscore before it (_Open) is a mneumonic. This means that it is for keyboard users. (alt+key)
If you remove this underscore add it back to another suitable character.

## Thanks for your efforts!
