## Libre Register
LibreRegister is an attempt at making a completely FOSS registration and timetabling program specificly orientated at secondary schools. It is currently in early development.

NOTE THIS PROJECT IS NOT EVEN AT A USABLE STAGE YET.

## Cloning
Clone with:

    git clone https://gitlab.com/john_t/libreregister

## Dependencies
There are quite a few dependencies for libre register:

    vala                    (build) (may also be called valac)
    gtk4                    (you might need a dev version if running debian or derivatives)
    gcc                     (build)
    meson                   (build)
    ninja                   (build)
    libgee
    glib2
    evolution-data-server
    libhandy4              (Note libhandy-4 is likely not available from your repo it can be cloned through https://gitlab.gnome.org/exalm/libhandy/-/tree/gtk4)
    json-glib
    gettext
    sqlite3

Note these are their names in the arch repos and aur.

## Building
Inside the libreregister directory run:

    meson . build --prefix=/usr
    cd build
    ninja

## Installing
You can install libreregister through ninja install.
