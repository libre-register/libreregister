/*
 * insert_contact_information.sql
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Just a peice of info. This is just a template, 
 * it is not an actual sql statement. Any '%%s' will be replaced in code,
 * in this case Vala.
 *
 */
INSERT INTO ContactInfo
	(pid, uuid, prefered, str, platform, type)
	VALUES (
		%s, -- pid			STRING
		%s, -- uuid			STRING
		%s, -- prefered		BOOL
		%s, -- str			STRING
		%s, -- platform		INT
		%s  -- type			INT
	)
;
