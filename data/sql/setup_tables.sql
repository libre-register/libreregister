/*
 * setup_tables.sql
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * Just a peice of info. This is just a template, 
 * it is not an actual sql statement. Any '%%s' will be replaced in code,
 * in this case Vala.
 *
 */
-- Login Table 
CREATE TABLE LogonUsers (
	uuid		CHAR(36)		PRIMARY KEY		NOT NULL,
	username	VARCHAR							NOT NULL,
	passwd		CHAR(128)						NOT NULL,
	permissions	INT								NOT NULL
);
-- Contact Table
CREATE TABLE Contacts (
	uuid			CHAR(36)	PRIMARY KEY		NOT NULL,
	-- Home PostBox
	home_po			TEXT,
	home_code		TEXT,
	home_country	TEXT,
	home_region		TEXT,
	home_locality	TEXT,
	home_ext		TEXT,

	-- Work PostBox
	work_po			TEXT,
	work_code		TEXT,
	work_country	TEXT,
	work_region		TEXT,
	work_locality	TEXT,
	work_ext		TEXT,

	-- Name
	prefixes		TEXT,
	given			TEXT,
	additional		TEXT,
	family			TEXT,

	-- Birthdate
	birthdate		TEXT, -- Note should be ISO8601 Formatted

	-- Work Information
	role			TEXT,
	org_unit		TEXT,
	office			TEXT,
	org				TEXT,
	title			TEXT,

	-- Image
	image			VARCHAR
);
-- ContactInfo Table
CREATE TABLE ContactInfo (
	pid			TEXT		PRIMARY KEY		NOT NULL,
	uuid		CHAR(36)					NOT NULL,
	prefered	BOOL						NOT NULL,
	str			TEXT						NOT NULL,
	/* Enum {
		EMAIL,
		TEL,
		X-SKYPE,
		X-AIM,
		X-GOOGLE-TALK,
		X-JABBER,
		X-MSN,
		X-YAHOO,
		X-ICQ,
		X-GROUPWISE,
		X-GADUGADU
	}
	*/
	platform INT,
	type	INT
);
-- Metadata table
CREATE TABLE Metadata (
	id		TEXT		PRIMARY KEY		NOT NULL,
	value	VARCHAR						NOT NULL
);
-- Set up Metadata
INSERT INTO Metadata (id, value) VALUES	('name',%s);
INSERT INTO Metadata (id, value) VALUES ('id',%s);
