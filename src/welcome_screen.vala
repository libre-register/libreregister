/*
 * welcome_screen.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gtk, Gee, E, Hdy, LrDB;

[GtkTemplate (ui = "/org/libre_register/LibreRegister/welcome.ui")]
public class WelcomeScreen : Gtk.ApplicationWindow {
	[GtkChild] private Button go_next;
	[GtkChild] private Button go_previous;
	[GtkChild] private Revealer next_revealer;
	[GtkChild] private Revealer previous_revealer;
	[GtkChild] private Stack stack;
	[GtkChild] private Button import_button;
	[GtkChild] private Button create_button;
	// Page 2
	[GtkChild] private Button image_button;
	[GtkChild] private Entry school_name_entry;
	// Page 3
	[GtkChild] private ListBox logon_listbox;
	// Logon Editor
	[GtkChild] private Entry username_entry;
	[GtkChild] private Entry password_entry;
	[GtkChild] private Entry confirm_password_entry;
	[GtkChild] private Label password_label;
	[GtkChild] private ComboBoxText permission_level_combobox;
	private LogonUserEditor active_logon;

	public DataBase db;
	public File dbfile;
	public Gdk.Pixbuf pixbuf;

	[GtkChild] private LevelBar finished_progress_bar;
	private bool writing {get; set; default=false;}
	construct {}
	[GtkCallback]
	private void hide_import_and_create () {
		create_button.hide ();
		import_button.hide ();
	}

	// Moves to the next page.
	[GtkCallback]
	private void next_page () {
		switch (stack.visible_child_name) {
			case ("logon_editor"):
				stack.visible_child_name = "4";
				break;
			case ("2"):
				create_database ();
				break;
			default:
				stack.visible_child_name = (int.parse (stack.visible_child_name) + 1).to_string ();
				break;
		}
		page_switch ();
	}

	// Moves to the previous page.
	[GtkCallback]
	private void prev_page () {
		stack.visible_child_name = (int.parse (stack.visible_child_name) - 1).to_string ();
		page_switch ();
	}

	private void page_switch () {
		go_next.show ();
		go_previous.show ();
		next_revealer.show ();
		previous_revealer.show ();
		switch (stack.visible_child_name) {
			case ("1"):
				next_revealer.reveal_child = false;
				previous_revealer.reveal_child = false;
				create_button.show ();
				import_button.show ();
				break;
			case ("2"):
				next_revealer.reveal_child = true;
				previous_revealer.reveal_child = true;
				break;
			case ("5"):
				next_revealer.reveal_child = false;
				break;
			case ("logon_editor"):
				next_revealer.reveal_child = true;
				previous_revealer.reveal_child = false;
				break;
			default:
				next_revealer.reveal_child = true;
				previous_revealer.reveal_child = true;
				break;
		}
	}

	// Initiates the user process of creating a database.
	private void create_database () {
		hide ();
		var filter = new FileFilter ();
		filter.add_pattern ("*.lrdb");
		filter.set_filter_name ("Libre Register Database");
		var file_chooser = new FileChooserNative (_("Save a File"), this, SAVE, _("_Save"), _("_Cancel"));
		file_chooser.add_filter (filter);
		file_chooser.response.connect ((response_id) => {
			// Hides the file chooser, the user has done all I require of them
			file_chooser.hide ();
			// Checks if the user actually wants to select
			if (response_id == ResponseType.ACCEPT) {
				dbfile = file_chooser.get_file ();
				show ();
				go_previous.show ();
				go_next.show ();
				stack.visible_child_name = "3";
			}
		});
		file_chooser.show ();
	}

	// Lets the user pick an image for the database
	[GtkCallback]
	private void select_image () {
		var file_chooser = new FileChooserNative ("Select Database Image", null, OPEN, _("_Open"), _("_Cancel"));
		FileFilter filter = new FileFilter ();
		filter.add_pixbuf_formats ();
		filter.name = "Image Files";
		file_chooser.filter = filter;
		file_chooser.response.connect ((response_id) => {
			// Hides the file chooser, the user has done all I require of them
			file_chooser.hide ();
			// Checks if the user actually wants to select
			if (response_id == ResponseType.ACCEPT) {
				try {
					var file = file_chooser.get_file ();
					pixbuf = new Gdk.Pixbuf.from_file_at_scale (file.get_path (), -1, 64, true);
					Image img = new Image.from_pixbuf (pixbuf);
					img.pixel_size = 64;
					image_button.set_child (img);
				} catch (Error e) {
					MessageDialog error_dialog = new MessageDialog (
						this,
						MODAL,
						ERROR,
						OK,
						e.message
					);
					error_dialog.response.connect (() => { error_dialog.hide (); });
					error_dialog.show ();
				}
			}
		});
		file_chooser.show ();
	}

	// Lets the user add a logon
	[GtkCallback]
	private void add_logon () {
		LogonUserEditor logon = new LogonUserEditor ();
		logon.more_clicked.connect (modify_logon);
		logon_listbox.prepend (logon);
	}

	// Lets the user remove a logon
	[GtkCallback]
	private void remove_logon () {
		foreach (ListBoxRow row in logon_listbox.get_selected_rows ()) {
			logon_listbox.remove (row);
		}
	}

	// When you modify a contact.
	private void modify_logon (LogonUserEditor logon_editor) {
		active_logon = logon_editor;
		stack.visible_child_name = "logon_editor";
		page_switch ();
		username_entry.text = logon_editor.get_username ();
		password_entry.text = "";
		confirm_password_entry.text = "";
		if (active_logon.logon_user!=null)
			permission_level_combobox.active = active_logon.logon_user.permissions;
	}

	// CHecks if passwords match and if so applies them
	[GtkCallback]
	private void check_passwords () {
		if (password_entry.text == confirm_password_entry.text) {
			password_label.label = "";
		} else {
			password_label.label = _("Passwords Do Not Match!");
		}
	}

	// Updates the password for the active logon
	[GtkCallback]
	private void update_password () {
		if (password_entry.text == confirm_password_entry.text) {
			password_label.label = "";
			Widget w = logon_listbox.get_first_child ();
			while ((LogonUserEditor) (w.get_first_child ()) != active_logon) {
				w = w.get_next_sibling ();
			}
			active_logon.check_create_contact ();
			active_logon.logon_user.change_passwd (password_entry.text, 0);
			active_logon.logon_user.permissions = permission_level_combobox.active;
		}
	}

	// Updates the username for the active logon
	[GtkCallback]
	private void update_username () {
		active_logon.set_username (username_entry.text);
		active_logon.update_contact ();
	}

	[GtkCallback]
	private void to_contact_editor () {
		ContactEditorWindow contact_editor = new ContactEditorWindow.from_contact (active_logon.contact);
		contact_editor.show ();
		contact_editor.contact_saved.connect ((contact) => {
			contact_editor.hide ();
			active_logon.contact = contact;
		});
	}

	[GtkCallback]
	private void write_to_file () {
		if (!writing) {
			writing = true;
			previous_revealer.reveal_child = true;
			print (_("Writing To Database file\n"));
			// Start setting up the database
			if (db == null) db = new DataBase (
					school_name_entry.text,
					school_name_entry.text.down ().replace (" ", "-"));
			db.name = school_name_entry.text;
			db.id = db.name.down ().replace (" ", "-");

			if (pixbuf != null) {
				db.image = pixbuf.read_pixel_bytes ().get_data ();
			}
			{
				// Adds Contact and Logon Users
				ListBoxRow w = (ListBoxRow) (logon_listbox.get_first_child ());
				while (w != null) {
					// Makes sure a Contact exists
					((LogonUserEditor) w.child).check_create_contact ();
					// Adds the logon user
					db.add_logon_user(((LogonUserEditor) w.child).logon_user);
					// Adds the contact
					db.add_contact (((LogonUserEditor) w.child).contact);
					// Moves forwatds in the linked list.
					w = (ListBoxRow) (w.get_next_sibling ());
				}
				// Checks if the database file exists
				if (dbfile.query_exists ())
					try {
						// Tries to deletes it
						dbfile.delete ();
					} catch (Error e) {
						// Throws an error.
						error (e.message);
					}
				// Lets a progress bar be used
				db.update_save_progress.connect(this.update_save_progress);
				// If threads aren't supported than we don't use them. This is very bad.
				if (!Thread.supported()) 
					db.write_to_new_file(dbfile.get_path ());
				// Otherwise we can use threads.
				else {
					print("Threads Supported.\n");
					// Preps it for thread use
					db.setup_threaded_file_write(dbfile.get_path ());
					// Creates a thread.
					Thread thread;
					thread = new Thread<void> (null, db.write_to_new_file_threaded);
				}
			}
		}
	}

	[GtkCallback]
	private void permissions_changed () {
		active_logon.check_create_contact ();
		active_logon.logon_user.permissions = permission_level_combobox.active;
	}
	private void update_save_progress(double d) {
		finished_progress_bar.value = d;
		if (d==1)
			writing=false;
	}
}
