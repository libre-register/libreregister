/*
 * contactable_editor.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foither version 2 of the License, or
 * (at your option) anyion.
 *
 * This program is distthe hope that it will be useful,
 * but WITHOUT ANY WARRut even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gtk, Gee, GLib, E;

[GtkTemplate (ui = "/org/libre_register/LibreRegister/contactable.ui")]
public class ContactableEditor : Frame {
	[GtkChild] private ComboBoxText messaging_platform_picker;
	[GtkChild] private ComboBoxText messaging_type_picker;
	[GtkChild] private ToggleButton starred_button;
	[GtkChild] private Entry entry;
	public string? uuid {get; set; default=GLib.Uuid.string_random();}
	public ContactableEditor () {
		Object ();
	}

	// A signal for when the type is changed.
	public signal void type_changed (string new_type_id) {
		switch (new_type_id) {
			case ("phone"):
				messaging_type_picker.remove_all ();
				messaging_type_picker.append ("landline", _("Landline"));
				messaging_type_picker.append ("work", _("Work"));
				messaging_type_picker.append ("mobile", _("Mobile"));
				break;
			// What is good for most cases
			default:
				messaging_type_picker.remove_all ();
				messaging_type_picker.append ("personal", "Personal");
				messaging_type_picker.append ("work", "Work");
				break;
		}
	}

	construct {
		// When the type is changed we emit the signal
		this.messaging_platform_picker.changed.connect (() => {
			type_changed (messaging_platform_picker.active_id);
		});
	}
	/// Creates a Contactable Editor from a Contact
	public ContactableEditor.from_contact_information(LrDB.ContactInformation ci) {
		this.entry.text = ci.contact_string;
		this.uuid = ci.uuid;
		this.starred_button.active = ci.preferability>=1;
		this.messaging_platform_picker.active = ci.platform;
		this.messaging_type_picker.active = ci.contact_type;
	}
	/// Updates this to a contact
	public void update_to_contact(LrDB.Contact contact) {
		contact.remove_contact_information_by_uuid(uuid); // Removes any previous instance of this on the contact
		LrDB.ContactInformation ci = new LrDB.ContactInformation(); // Creates a new Contact Information
		// Begins to fill in its details
		ci.uuid = uuid;
		ci.preferability = 0;
		if (starred_button.active)
			ci.preferability = 1;
		ci.platform = (LrDB.ContactInformationPlatform) messaging_platform_picker.active; 
		ci.contact_type = (LrDB.ContactInformationType) messaging_type_picker.active;
		ci.contact_string = entry.text;
		contact.add_contact_information(ci);
	}
}
