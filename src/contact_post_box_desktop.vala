/*
 * contact_post_box_desktop.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 * I would just like to say libHanDY is the best! 🤩
 *
 */

using Gtk, E, Hdy;

[GtkTemplate (ui = "/org/libre_register/LibreRegister/post_box_desktop.ui")]
public class DesktopPostBox : Grid, PostBoxEditor {
	// Some Postbox related fields
	[GtkChild] private TextView address_text_view;
	[GtkChild] private Entry postbox_entry;
	[GtkChild] private Entry city_entry;
	[GtkChild] private Entry county_entry;
	[GtkChild] private Entry country_entry;
	[GtkChild] private Entry zip_entry;

	public TextView get_address_text_view () {
		return address_text_view;
	}

	public Entry get_postbox_entry () {
		return postbox_entry;
	}

	public Entry get_city_entry () {
		return city_entry;
	}

	public Entry get_county_entry () {
		return county_entry;
	}

	public Entry get_country_entry () {
		return country_entry;
	}

	public Entry get_zip_entry () {
		return zip_entry;
	}
}
