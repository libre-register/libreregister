
/*
 * lr_timetabling_session.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

using GLib, Gee;

namespace LrTB {
	/**
	 * Session is a class to store when a class takes place
	 */
	public class Session : Object {
		public int day  { get; set; default = 0; }
		public int session  { get; set; default = 0; }
		public int days;
		public int sessions;
		public Session (int day = 0, int session = 0) {
			Object (day: day, session: session);
		}

		public void add (int sessions_to_add) {
			if (days != 0 && sessions != 0) {
				session += sessions_to_add;
				day += (int) Math.floor (session / sessions);
				session = session % sessions;
				day = day % days;
			} else critical ("no timetable ranges specified, days, sessions");
		}
	}
}
