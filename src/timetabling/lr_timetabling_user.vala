/*
 * lr_timtabling_user.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gee, GLib;

namespace LrTB {
	public class User : Object {
		/**
		 * The string to be used to identify a contact associated with this object.
		 */
		public string uuid;
		/**
		 * This is a list of all the user's qualifications
		 */
		public ArrayList<Qualification> qualifications;
		/**
		 * The root group of the user.
		 * It is, in practice, the same as the timetable.
		 */
		public Group? root_group
		{ owned get {
			  foreach (Group group in groups)
				  if (group.parent_group == null)
					  return group;
			  return null;
		  } }
		/**
		 * The timetable of the user.
		 * In practice it is always the root group.
		 */
		public TimeTable? timetable																																					 { owned get {
																																															  return (TimeTable) root_group;
																																														  } }
		public ArrayList<Group> groups																																  { get; private set; }
		public ArrayList<ClassRole> classes_to_take																					   { get; private set; }
		public ArrayList<Grade> grades;
		// I question my sanity
		public ArrayList<ArrayList<ArrayList<Class? > > > timetabled_classes   { get; private set; }
		construct {
			timetabled_classes = new ArrayList<ArrayList<ArrayList<Class> > >();
			for (int i = 0; i < timetable.days; i++) {
				timetabled_classes.add (new ArrayList<ArrayList<Class? > >());
				for (int j = 0; j < timetable.sessions_per_day; j++)
					timetabled_classes[i].add (new ArrayList<Class? >());
			}
			groups = new ArrayList<Group>();
			classes_to_take = new ArrayList<ClassRole>();
			grades = new ArrayList<Grade>();
		}
		/**
		 * Checks if a certain class is at an applicable time for the user.
		 */
		public bool can_attend_lesson (int day, int session) {
			if (day < timetable.days && session < timetable.sessions_per_day) {
				return (timetabled_classes[day][session].size == 0);
			}
			return false;
		}

		/**
		 * Checks if a certain session is an applicable time for the user
		 */
		public bool can_attend_session (Session session) {
			return can_attend_lesson (session.day, session.session);
		}

		/**
		 * Checks if a user can attend a specific class
		 */
		public bool can_attend_class (Class class_to_attend) {
			foreach (Session session in class_to_attend.sessions) if (!can_attend_session (session)) return false;
			return true;
		}

		/**
		 * Tries to add a user to a class
		 *
		 * Throws an error if the user already has a class at the given time
		 */
		public void add_to_class_as_student (Class class_to_join) throws TimeTablingError {
			// Checks if we can even attend this class
			if (!can_attend_class (class_to_join))
				throw new TimeTablingError.SLOT_NOT_EMPTY ("The requested student already has a class!");
			else {
				foreach (Session session in class_to_join.sessions) {
					// Adds this to our timetable
					this.timetabled_classes[session.day][session.session].add (class_to_join);
					class_to_join.students.add (this);
				}
			}
		}

		/**
		 * Adds a user to a class even if it makes their life a nightmare.
		 */
		public void force_add_to_class_as_student (Class klass) {
			foreach (Session session in klass.sessions) {
				// Adds this to our timetable
				for (int i = 0; i < timetable.days; i++) {
					for (int j = 0; j < timetable.sessions_per_day; j++) {
						if (timetabled_classes[session.day][session.session].contains (klass))
							timetabled_classes[session.day][session.session].remove (klass);
					}
				}
				klass.students.add (this);
				this.timetabled_classes[session.day][session.session].add (klass);
			}
		}

		/**
		 * Gives the users grade for a certain subject
		 */
		public double give_grade (string id, GradeScoreMethod score_method = MOST_RECENT) {
			ArrayList<Grade> applicable_grades = give_grades_for_subject (id);
			return new Grade ().give_grade (applicable_grades, score_method);
		}

		/**
		 * Gets all the relevant grades for a subject
		 */
		public ArrayList<Grade> give_grades_for_subject (string id) {
			var applicable_grades = new ArrayList<Grade>();
			foreach (Grade grade in this.grades) {
				if (grade.id == id) applicable_grades.add (grade);
			}
			return applicable_grades;
		}

		/**
		 * Checks if a user has a qualification greater or equal to the given onw
		 */
		public bool has_qualification (Qualification qualification) {
			foreach (Qualification quali in qualifications) {
				if (quali.id == qualification.id &&
					quali.level >= qualification.level)
					return true;
			}
			return false;
		}

		/**
		 * Gets the last sceduled session in the users timetable
		 */
		public Session get_next_free_space () {
			for (int day = 0;
				 day < timetabled_classes.size;
				 day++) {
				for (int session = 0;
					 session < timetabled_classes[0].size;
					 session++) {
					if (timetabled_classes[day][session].size == 0)
						continue;
					else {
						Session s = new Session ();
						s.day = day;
						s.session = session;
						return s;
					}
				}
			}
			return new Session (0, 0);
		}
	}
}
