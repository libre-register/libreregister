/*
 * lr_timetable_class_role.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gee, GLib;

namespace LrTB {
	/**
	 *
	 */
	public class ClassRole : Object {
		/**
		 * The minimum qualification to teach the class.
		 */
		public Qualification minimum_qualification;
		/**
		 * The group (or subgroup) that takes this class.
		 */
		public ArrayList<Group> groups;
		/**
		 * How many sessions per timetable does this classrole need
		 */
		public int sessions_per_loop { get; set; default = 1; }

		construct {
			groups = new ArrayList<Group>();
		}
		public bool user_is_applicable (User user) {
			foreach (Group group in groups) if (group.users.contains (user)) return true;
			return false;
		}
	}
}
