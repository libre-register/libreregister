/*
 * lr_timetable_grades.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

using GLib, Gee;

namespace LrTB {
	/**
	 * A class to store an individual grade
	 */
	public class Grade : Object {
		public string id;
		public double grade;
		public Date date;
		/**
		 * Gives the average grade from a list.
		 */
		public static double give_grade (Gee.List<Grade> grades, GradeScoreMethod method) {
			switch (method) {
				case (MEAN):
					double running_total = 0;
					foreach (Grade grade in grades)
						running_total += grade.grade;
					return running_total / grades.size;
				case (MEDIAN):
					grades.sort ((a, b) => {
					return a.date.compare (b.date);
				});
					if (grades.size % 2 == 0) {
						var n1 = grades[(int) Math.floor ((grades.size - 1) / 2)];
						var n2 = grades[(int) Math.ceil ((grades.size - 1) / 2)];
						return (n1.grade + n2.grade) / 2;
					} else {
						return grades[(grades.size - 1) / 2].grade;
					}
				case (MOST_RECENT):
					return grades[grades.size - 1].grade;
				case (MEAN_HALF):
					return give_grade (grades[: (int) Math.ceil (grades.size / 2)], MEAN);
				case (MEDIAN_HALF):
					return give_grade (grades[: (int) Math.ceil (grades.size / 2)], MEDIAN);
				case (MEAN_DIMINISHING):
					int i = 0;
					double running_total = 0;
					double div_total = 0;
					foreach (Grade grade in grades) {
						i++;
						running_total += grade.grade * (1 / i);
						div_total += 1 / i;
					}
					return running_total / div_total;
			}
			return 0;
		}
	}
}
