/*
 * lr_timetabling_timetable.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * aint with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

using GLib, Gee;

namespace LrTB {
	/**
	 * The base class which stores all other classes.
	 * It inheirates from group, and so can store subgroups and users
	 */
	public class TimeTable : Group {
		/**
		 * How many days until the timetable loops (normally 5 for a real schoo;)
		 */
		public int days { get; construct; default = 5; }
		/**
		 * How many lessons are there a day (not including lunch)
		 */
		public int sessions_per_day { get; construct; default = 6; }
		public int sessions_per_loop { get {
										   return sessions_per_day * days;
									   } }
		/**
		 * Creates a timetable
		 * @p days how many days until the timtable loops.
		 * @p sessions_per_day how many lessons are there per day
		 */
		public TimeTable (int days, int sessions_per_day) {
			Object (days: days, sessions_per_day: sessions_per_day);
		}

		construct {
			if (prefered_students_per_class == null)
				prefered_students_per_class = 30;
		}
		/**
		 * Tries to generate a timetable
		 *
		 * @throws TimeTablingError throws an error if cannot generate timetable
		 */
		public void generate_timetable () throws TimeTablingError {
			try {
				manage ();
			} catch (TimeTablingError e) {
				throw (e);				 // Just gotta dump my problems on the user
			}
		}

		/**
		 * Gets the amount of overlap classes at one time.
		 * I do not want users to use this.
		 */
		internal long get_overlap_classes () {
			// Out goes the preformance
			long session_total = get_sessions ();
			if (session_total % sessions_per_loop != 0)
				warning ("SOME USERS WILL HAVE BLANK PERIODS");
			return (long) Math.ceil (session_total / sessions_per_loop);
		}
	}
}
