/*
 * lr_timetabling_class.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gee, GLib;

namespace LrTB {
	public class Class : Group {
		/**
		 * The minimum qualification needed for the class.
		 */
		public Qualification min_qualification;

		/**
		 * The students in the class
		 */
		public ArrayList<User> students { get; private set; }
		/**
		 * The teachers in the class
		 */
		public ArrayList<User> teachers { get; private set; }
		/**
		 * The session which the class takes place
		 */
		public ArrayList<Session> sessions;
		/**
		 * It is not too helpful to get the users, as it just returens students anyway
		 */
		construct {
			students = new ArrayList<User>();
			teachers = new ArrayList<User>();
			sessions = new ArrayList<Session>();
		}
		/**
		 * Adds a student to this.
		 *
		 * @p user The user to add to this
		 */
		public void add_student (User user) throws TimeTablingError {
			try {
				user.add_to_class_as_student (this);
				add_user (user);
			} catch (TimeTablingError e) {
				throw e;
			}
		}

		/**
		 * Adds a student to this.
		 *
		 * @p user The user to add to this
		 */
		public void add_teacher (User user) throws TimeTablingError {
			if (user.can_attend_class (this)) {
				teachers.add (user);
				foreach (Session session in sessions)
					user.timetabled_classes[session.day][session.session].add (this);
				add_user (user);
			} else {
				throw (new TimeTablingError.SLOT_NOT_EMPTY ("The requested teacher already has a class!"));
			}
		}

		/**
		 * Sorts all the members of a group by a id.
		 */
		public ArrayList<User> give_qualification_users (string id) {
			ArrayList<User> users = new ArrayList<User>();
			foreach (User user in users) {
				foreach (Qualification qualification in user.qualifications) {
					if (qualification.id == id) {
						users.add (user);
						break;
					}
				}
			}
			// Sorts the users by qualification
			users.sort ((a, b) => {
				int la = -int.MAX;
				foreach (Qualification qualification in a.qualifications) {
					if (qualification.id == id) {
						la = qualification.level;
						break;
					}
				}
				int lb = -int.MAX;
				foreach (Qualification qualification in b.qualifications) {
					if (qualification.id == id) {
						lb = qualification.level;
						break;
					}
				}
				if (la > lb) return -1;
				if (lb > la) return +1;
				return 0;
			});
			return users;
		}

		/**
		 * Returns a sorted list of all qualifications above a certain level
		 */
		public ArrayList<User> give_min_qualification_users (Qualification qualification) {
			var arr = give_qualification_users (qualification.id);
			var arr2 = new ArrayList<User>();
			foreach (User user in arr) {
				foreach (Qualification u_qualification in user.qualifications) {
					if (u_qualification.id == qualification.id) {
						if (u_qualification.level >= qualification.level) {
							arr2.add (user);
							continue;
						}
						break;
					}
				}
			}
			return arr2;
		}

		/**
		 * Returns a list of users which have got other classes at the same time
		 */
		public ArrayList<User> give_broken_users () {
			ArrayList<User> broken_users = new ArrayList<User>();
			foreach (Session session in sessions) {
				foreach (User user in users) {
					if (user.timetabled_classes[session.day][session.session].size > 1)
						broken_users.add (user);
				}
			}
			broken_users.sort ((a, b) => {
				var bn = 0;
				var an = 0;
				foreach (Session session in sessions)
					an += a.timetabled_classes[session.day][session.session].size;
				foreach (Session session in sessions)
					bn += b.timetabled_classes[session.day][session.session].size;
				if (an > bn) return -1;
				if (bn > an) return 1;
				return 0;
			});
			return broken_users;
		}

		/**
		 * Returns the least broken user which is still broken
		 */
		public User? give_least_broken_user () {
			var u = give_broken_users ();
			if (u != null && u.size >= 1) return u[0];
			return null;
		}
		/**
		 * Adds a list of students to the class
		 */
		public void add_students (Gee.List<User> list) throws TimeTablingError {
			foreach (User user in list) {
				add_student (user);
			}
		}

		/**
		 * Adds a session to the class
		 */
		public void add_session (Session session) {
			sessions.add (session);
		}
	}
}
