/*
 * lr_timetable_groups.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

using GLib, Gee;

namespace LrTB {
	/**
	 * These are happy timetable related errors. So fun :(.
	 */
	public errordomain TimeTablingError {
		/// This signifies that there are more lessons a student must take than lessons available.
		NO_SLOTS_LEFT,
		/// This means there are no teachers qualified to teach a lesson. Get a better school. ·_·
		NO_APPLICABLE_TEACHERS,
		/// This means the user already has a class in a certain slot
		SLOT_NOT_EMPTY,
		/// This means a subgroup already has a parent
		SUBGROUP_HAS_PARENT
	}
	public class Group : Object {
		/**
		 * A list of everyone in the group
		 */
		public virtual ArrayList<User> users { get; construct; }
		/**
		 * The name of the group
		 */
		public string name;
		/**
		 * The parent group of this
		 */
		public Group parent_group;
		/**
		 * How many students per class. Note it can be changed on a lower level.
		 */
		public long? prefered_students_per_class;
		/**
		 * The Classes in this group
		 */
		public ArrayList<Class> classes										   { get; private set; }
		public ArrayList<Group> subgroups												 { get; private set; }
		public ArrayList<ClassRole> classes_to_take	   { get; private set; }
		public Group root_group { owned get {
									  Group parent = this;
									  while (parent.parent_group != null) parent = parent.parent_group;
									  return parent;
								  } }
		public TimeTable root_timetable { owned get {
											  return (TimeTable) root_group;
										  } }
		construct {
			classes = new ArrayList<Class>();
			subgroups = new ArrayList<Group>();
			classes_to_take = new ArrayList<ClassRole>();
			users = new ArrayList<User>();
		}
		/**
		 * A function to add a subgroup to this
		 * @p group The group to add.
		 */
		public void add_subgroup (Group group) throws Error {
			if (group.parent_group != null) {
				throw new TimeTablingError.SUBGROUP_HAS_PARENT ("Subgroup has parent!");
			} else {
				group.parent_group = this;
				subgroups.add (group);
			}
		}

		/**
		 * Adds A user to this.
		 * @p user The user to add.
		 */
		public void add_user (User user) requires (!users.contains (user)) {
			// Adds the user
			this.users.add (user);
			// Adds this to the user
			user.groups.add (this);
			if (this.parent_group != null) parent_group.add_user (user);
		}
		/**
		 * Adds A class role which everyone in the group will be required to take when timetables are made.
		 * @p class_role thee class role to add to this
		 */
		public void add_class_role (ClassRole class_role) {
			this.classes_to_take.add (class_role);
			class_role.groups.add (this);
			foreach (User user in users)
				user.classes_to_take.add (class_role);
		}

		/**
		 * Manages a small subsection of the timetable
		 */
		public void manage () throws TimeTablingError {
			// manage ourselves
			classes_to_take.sort ((a, b) => {
				int al = a.minimum_qualification.level;
				int bl = b.minimum_qualification.level;
				if (al == bl) return 0;
				if (al > bl) return -1;
				return 1;
			});
			// Loops through the now sorted list
			var classes = new ArrayList<ArrayList<Class> >();
			foreach (ClassRole role in classes_to_take) {
				classes.add (new ArrayList<Class>());
				sort_users_by_grade (MEAN_DIMINISHING, role.minimum_qualification.id);
				var class_base = split_users_by_classrole (role);				 // Ok Great. Now we have some 'ideas' and now it is time to timetable them.
				// Lets turn these into basic classes
				foreach (ArrayList<User> list in class_base) {
					Class klass = new Class ();
					klass.add_students (list);
					classes.last ().add (klass);
				}
			}
			// Now allocates them on a timetable
			int i = 0;
			foreach (ArrayList<Class> klassr in classes) {
				// We wan't to use only the teachers with the bear minimum qualification
				ClassRole role = classes_to_take[i];
				var teachers = get_teachers_with_qualification (role.minimum_qualification);
				int klassn = 0;
				// Goes through each class role's class
				foreach (Class klass in klassr) {
					Session session = new Session ();
					// All users in a Class should be identical.
					User sample = klass.users[0];
					session = sample.get_next_free_space ();
					session.days = root_timetable.days;
					session.sessions = root_timetable.sessions_per_day;
					session.add (klassn / (int) root_timetable.get_overlap_classes ());
					klassn++;
				}
				i++;
			}
			// Then manage all subgroups
			try {
				foreach (Group group in subgroups) {
					group.manage ();
				}
			} catch (TimeTablingError e) {
				throw (e);
			}
		}

		/**
		 * This sorts the users by their grades
		 */
		public void sort_users_by_grade (GradeScoreMethod score_method, string id) {
			users.sort ((a, b) => {
				double b_grade = b.give_grade (id, score_method);
				double a_grade = a.give_grade (id, score_method);
				if (a_grade > b_grade) return -1;
				if (b_grade > a_grade) return 1;
				return 0;
			});
		}

		/**
		 * Gets all the teachers who have a qualification
		 * Note should be run on a toplevel (normally a timetable)
		 */
		public ArrayList<User> get_teachers_with_qualification (Qualification cuttoff_point) {
			var applicable_users = new ArrayList<User>();
			foreach (User user in users) {
				if (user.has_qualification (cuttoff_point)) {
					applicable_users.add (user);
				}
			}
			return applicable_users;
		}

		/**
		 * Gets a lsit of all the users for a class role split into sections
		 */
		public ArrayList<ArrayList<User> > split_users_by_classrole (ClassRole class_role) {
			// Our list of students
			var list = new ArrayList<ArrayList<User> >();
			var students = new ArrayList<User>.wrap (users.to_array ());

			long prefered_amount = get_prefered_students_per_class_from_parent ();
			long class_count = (long) Math.ceil ((double) users.size / prefered_amount);
			for (int i = 0; i < class_count; i++) {
				var list_section = new ArrayList<User>();
				if (i + 1 == class_count) {
					list_section = students;
				} else {
					// Adds the first students to the class
					for (int j = 0; j < (int) Math.round (users.size / class_count); j++) {
						list_section.add (students[0]);
						students.remove_at (0);
					}
				}
				list.add (list_section);
			}
			return list;
		}

		/**
		 * Gets the perefered students per class either from this or from its parent;
		 */
		public long get_prefered_students_per_class_from_parent () {
			if (prefered_students_per_class != null)
				return prefered_students_per_class;
			else
				return parent_group.get_prefered_students_per_class_from_parent ();
		}

		/**
		 * Gets the amount of sessions in this, and all subgroup's classroles.
		 * One session per student
		 */
		public long get_sessions (bool recurse = true) {
			long sessions = 0;
			long prefered_amount = get_prefered_students_per_class_from_parent ();
			long class_count = (long) Math.ceil ((double) users.size / prefered_amount);
			sessions += classes_to_take.size * class_count;
			if (recurse) {
				foreach (Group subgroup in subgroups) {
					sessions += subgroup.get_sessions ();
				}
			}
			return sessions;
		}
	}
}
