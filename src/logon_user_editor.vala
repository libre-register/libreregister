/*
 * logon_user_editor.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gtk, Gee, GLib, E, LrDB;
[GtkTemplate (ui = "/org/libre_register/LibreRegister/logon_user.ui")]
public class LogonUserEditor : Frame {
	[GtkChild] private Label name_label;
	[GtkChild] private Label username_label;
	[GtkChild] private Button more_button;
	private LrDB.Contact _contact;
	public LrDB.Contact contact
		{ owned get {
			return _get_contact ();
		} set {
			restore_from_contact (value);
		} }
	public LrDB.LogonUser logon_user;
	public signal void more_clicked (LogonUserEditor widget);

	construct {
		more_button.clicked.connect (() => { more_clicked (this); });
	}
	// To restore from a contact
	public void restore_from_contact (LrDB.Contact contact) {
		_contact = contact;
		update_contact ();
	}

	// Updates the contact
	public void update_contact () {
		if (contact != null) {
			if (contact.name != null) {
				var name = contact.name;
				name_label.label = name.prefixes + " " + name.given + " " + name.additional + " " + name.family;
			}
			username_label.label = get_username ();
		}
	}

	// Gets the contact
	private LrDB.Contact _get_contact () {
		check_create_contact ();
		((VCard) _contact).util_set_x_attribute ("X-LIBREREGISTER-USERNAME", get_username ());
		return _contact;
	}

	// Sets the username
	public void set_username (string username) {
		check_create_contact ();
		((VCard) _contact).util_set_x_attribute ("X-LIBREREGISTER-USERNAME", username);
	}

	// Gets the username, if none exists than it auto generates a pretty bad one!
	public string get_username () {

		var username_attribute = ((VCard) _contact).get_attribute ("X-LIBREREGISTER-USERNAME");
		string username;
		if (username_attribute == null) {
			username = name_label.label.down ();
			username = username.replace (" ", "_");
			username = username.replace ("-", "_");
			username = username.strip ();
		} else username = username_attribute.get_value ();
		return username;
	}

	// Runs checks, such as creating a contact and such the like.
	public void check_create_contact () {
		if (_contact == null) {
			_contact = new LrDB.Contact ();
			_contact.id = Uuid.string_random ();
		}
		if (logon_user == null) logon_user = new LrDB.LogonUser ();

		logon_user.username = get_username ();
		logon_user.uuid = _contact.id;
	}
}
