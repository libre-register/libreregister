/*
 * libre_register.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

using Gtk, Gee, GLib;

// The main class for this Application.
public class LibreRegisterApplication : Gtk.Application {
	WelcomeScreen welcome_screen;
	public LibreRegisterApplication () {
		// Creates an Application with the required ID.
		Object (application_id: "org.libre_register.LibreRegister", flags : ApplicationFlags.FLAGS_NONE);
	}

	public override void activate () {
		IconTheme icon_theme = IconTheme.get_for_display (Gdk.Display.get_default ());
		icon_theme.add_resource_path ("/org/libre_register/LibreRegister/icons");
		// Creates a ContactEditorWindow.
		// Adds it to the list of windows this Application manages.
		// Shows the window (temp)

		welcome_screen = new WelcomeScreen ();
		add_window (welcome_screen);
		welcome_screen.show ();
	}
}

public static int main (string[] args) {
	GLib.Intl.bindtextdomain ("org.libre_register.LibreRegister", null);
	GLib.Intl.bind_textdomain_codeset ("org.libre_register.LibreRegister", null);
	GLib.Intl.textdomain ("org.libre_register.LibreRegister");
	GLib.Intl.setlocale (GLib.LocaleCategory.ALL, null);
	// Creates a new Application.
	var app = new LibreRegisterApplication ();
	// Runs the program.
	return app.run (args);
}
