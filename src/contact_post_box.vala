/*
 * contact_post_box.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gtk;
public interface PostBoxEditor : Object {
	// Some Postbox related fields
	public abstract TextView get_address_text_view ();
	public abstract Entry get_postbox_entry ();
	public abstract Entry get_city_entry ();
	public abstract Entry get_county_entry ();
	public abstract Entry get_country_entry ();
	public abstract Entry get_zip_entry ();
}
