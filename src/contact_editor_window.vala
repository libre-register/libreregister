/*
 * contact_editor_window.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gtk, Gee, GLib, E, Hdy;

// The main window for the contact editor
[GtkTemplate (ui = "/org/libre_register/LibreRegister/contact_editor.glade")]
public class ContactEditorWindow : Gtk.ApplicationWindow {
	// Some stack related fields
	[GtkChild] private ViewSwitcherBar view_switcher;
	[GtkChild] private Leaflet leaflet;
	[GtkChild] private Stack stack;

	// The revealer for the birthday.
	[GtkChild] private Revealer birthdate_revealer;

	// Some name related fields
	[GtkChild] private Entry first_name_entry;
	[GtkChild] private Entry last_name_entry;
	[GtkChild] private Entry salutation_name_entry;
	[GtkChild] private Entry middle_name_entry;

	// Some Birthday related fields
	[GtkChild] private SpinButton birthdate_year_spinbutton;
	[GtkChild] private SpinButton birthdate_day_spinbutton;
	[GtkChild] private SpinButton birthdate_month_spinbutton;
	[GtkChild] private Switch birthday_switch;

	// Some Post Box related fields

	[GtkChild] private Squeezer post_box_squeezer;
	// Desktop View
	[GtkChild] private DesktopPostBox work_po_box_grid_desktop;
	[GtkChild] private DesktopPostBox home_po_box_grid_desktop;
	// Mobile View
	[GtkChild] private MobilePostBox work_po_box_grid_mobile;
	[GtkChild] private MobilePostBox home_po_box_grid_mobile;

	private TextView work_address_text_view {
		owned get {
			return get_work_po_box ().get_address_text_view ();
		}
	}
	private Entry work_postbox_entry {
		owned get {
			return get_work_po_box ().get_postbox_entry ();
		}
	}
	private Entry work_city_entry {
		owned get {
			return get_work_po_box ().get_city_entry ();
		}
	}
	private Entry work_county_entry {
		owned get {
			return get_work_po_box ().get_county_entry ();
		}
	}
	private Entry work_country_entry {
		owned get {
			return get_work_po_box ().get_country_entry ();
		}
	}
	private Entry work_zip_entry {
		owned get {
			return get_work_po_box ().get_zip_entry ();
		}
	}

	private TextView home_address_text_view {
		owned get {
			return get_home_po_box ().get_address_text_view ();
		}
	}
	private Entry home_postbox_entry {
		owned get {
			return get_home_po_box ().get_postbox_entry ();
		}
	}
	private Entry home_city_entry {
		owned get {
			return get_home_po_box ().get_city_entry ();
		}
	}
	private Entry home_county_entry  {
		owned get {
			return get_home_po_box ().get_county_entry ();
		}
	}
	private Entry home_country_entry {
		owned get {
			return get_home_po_box ().get_country_entry ();
		}
	}
	private Entry home_zip_entry  {
		owned get {
			return get_home_po_box ().get_zip_entry ();
		}
	}

	// Some Work related fields
	[GtkChild] private Entry job_title_entry;
	[GtkChild] private Entry job_role_entry;
	[GtkChild] private Entry organisation_entry;
	[GtkChild] private Entry sub_unit_entry;
	[GtkChild] private Entry unit_entry;

	// Some buttons
	[GtkChild] private Button save_as_button;
	[GtkChild] private Button save_button;

	// Some contact information fields
	[GtkChild] private ListBox contact_listbox;
	[GtkChild] private Button contact_add_button;
	[GtkChild] private Button contact_remove_button;

	private LrDB.Contact contact;
	// Makes a new LrDB.ContactEditor Window.
	public signal void contact_saved (LrDB.Contact contact);

	public ContactEditorWindow () {
	}

	/**
	 * Creates a ContactEditorWindow from the information in a contact.
	 */
	public ContactEditorWindow.from_contact (LrDB.Contact contact) {
		this();
		// Sets up the required classes
		ContactName name = contact.name;
		ContactDate birthdate = contact.birth_date;
		ContactAddress home_addr = contact.address_home;
		ContactAddress work_addr = contact.address_work;

		// Sets up the more advance names
		if (name != null) {
			first_name_entry.text = name.given;
			last_name_entry.text = name.family;
			middle_name_entry.text = name.additional;
			salutation_name_entry.text = name.prefixes;
		}

		// Sets up the Birthdate
		if (contact.birth_date != null) {
			birthdate_year_spinbutton.set_value (birthdate.year);
			birthdate_month_spinbutton.set_value (birthdate.month);
			birthdate_day_spinbutton.set_value (birthdate.day);
			birthday_switch.active = true;
		} else birthday_switch.active = false;

		// Sets up PO Boxes
		if (work_addr != null) {
			work_address_text_view.buffer.text = work_addr.po;
			work_zip_entry.text = work_addr.code;
			work_country_entry.text = work_addr.country;
			work_county_entry.text = work_addr.region;
			work_city_entry.text = work_addr.locality;
		}
		if (home_addr != null) {
			home_address_text_view.buffer.text = home_addr.po;
			home_zip_entry.text = home_addr.code;
			home_country_entry.text = home_addr.country;
			home_county_entry.text = home_addr.region;
			home_city_entry.text = home_addr.locality;
		}

		// Sets up some work informaion
		if (contact.role != null) job_role_entry.text = contact.role;
		if (contact.org_unit != null) unit_entry.text = contact.org_unit;
		if (contact.office != null) sub_unit_entry.text = contact.office;
		if (contact.org != null) organisation_entry.text = contact.org;
		if (contact.title != null) job_title_entry.text = contact.title;

		foreach (var ci in contact.get_contact_information(null, null)) {
			contact_listbox.insert(
				new ContactableEditor.from_contact_information(ci), -1
			);
		}
		this.contact = contact;
	}

	construct {
		// lets you save things
		save_as_button.clicked.connect (save_contact);

		// Connects the changing of the switch to a function.
		birthday_switch.notify.connect (birthday_switch_changed);
		birthday_switch.set_active(false);

		// Connects add and remove buttons to their respective functions.
		contact_add_button.clicked.connect (add_contact);
		contact_remove_button.clicked.connect (remove_contact);

		leaflet.notify.connect (leaflet_changed);

		// Makes the post box adaptive
		post_box_squeezer.notify.connect ((pspec) => { if (pspec.name == "visible-child") restore_post_box (); });

		save_button.clicked.connect (() => {
			contact_saved (get_contact ());
		});
	}
	// To get the active work postbox
	private PostBoxEditor get_work_po_box () {
		PostBoxEditor x;
		if (post_box_squeezer.visible_child.name == "mobile")
			x = work_po_box_grid_mobile;
		else
			x = work_po_box_grid_desktop;
		return x;
	}

	// To get the active home postbox
	private PostBoxEditor get_home_po_box () {
		PostBoxEditor x;
		if (post_box_squeezer.visible_child.name == "mobile")
			x = home_po_box_grid_mobile;
		else
			x = home_po_box_grid_desktop;
		return x;
	}

	private void restore_post_box () {
		// Old home
		PostBoxEditor old_home;
		if (post_box_squeezer.visible_child.name == "desktop")
			old_home = home_po_box_grid_mobile;
		else
			old_home = home_po_box_grid_desktop;
		// Old work
		PostBoxEditor old_work;
		if (post_box_squeezer.visible_child.name == "desktop")
			old_work = work_po_box_grid_mobile;
		else
			old_work = work_po_box_grid_desktop;
		// New
		PostBoxEditor new_home = get_home_po_box ();
		PostBoxEditor new_work = get_work_po_box ();

		// New Home
		new_home.get_address_text_view ().get_buffer ().text = old_home.get_address_text_view ().get_buffer ().text;
		new_home.get_postbox_entry ().text = old_home.get_postbox_entry ().text;
		new_home.get_city_entry ().text = old_home.get_city_entry ().text;
		new_home.get_county_entry ().text = old_home.get_county_entry ().text;
		new_home.get_country_entry ().text = old_home.get_country_entry ().text;
		new_home.get_zip_entry ().text = old_home.get_zip_entry ().text;

		// New Work
		new_work.get_address_text_view ().get_buffer ().text = old_work.get_address_text_view ().get_buffer ().text;
		new_work.get_postbox_entry ().text = old_work.get_postbox_entry ().text;
		new_work.get_city_entry ().text = old_work.get_city_entry ().text;
		new_work.get_county_entry ().text = old_work.get_county_entry ().text;
		new_work.get_country_entry ().text = old_work.get_country_entry ().text;
		new_work.get_zip_entry ().text = old_work.get_zip_entry ().text;
	}

	// To make the interface responsive
	private void leaflet_changed (ParamSpec pspec) {
		if (pspec.get_name () == "folded") {
			view_switcher.reveal = leaflet.folded;
			if (leaflet.folded)
				stack.transition_type = SLIDE_LEFT_RIGHT;
			else
				stack.transition_type = SLIDE_UP_DOWN;
		}
	}

	private void add_contact () {
		var contactable_editor = new ContactableEditor ();
		contact_listbox.insert (contactable_editor, -1);
	}

	private void remove_contact () {
		contact_listbox.selected_foreach ((box, child) => { box.remove (child); });
	}

	// Reacts to when the birthday switch gets flipped
	private void birthday_switch_changed () {
		// Sets the revealers state to be that of the switch
		birthdate_revealer.set_reveal_child (birthday_switch.active);
	}

	private void save_contact () {
		// The file chooser to pick a file to save to. It's just a file chooser!
		var file_chooser = new FileChooserNative (_("Save Contact"), this, SAVE, _("_Save"), _("_Cancel"));
		file_chooser.set_current_name (_("MyLrDB.Contactvcf"));
		file_chooser.response.connect ((response_id) => {
			// Hides the file chooser, the user has done all i require of them
			file_chooser.hide ();
			// Checks if the user actually wants to save
			if (response_id == ResponseType.ACCEPT) {
				try {
					var file = file_chooser.get_file ();
					bool can_write = true;
					if (file.query_exists ()) {
						file.delete ();
					}
					// If the user wants us to write to the file
					if (can_write) {
						DataOutputStream dos = new DataOutputStream (file.create (FileCreateFlags.REPLACE_DESTINATION));
						dos.put_string (get_contact ().to_string (@30));
					}
				} catch (Error e) {
					MessageDialog error_dialog = new MessageDialog (
						this,
						MODAL,
						ERROR,
						OK,
						e.message
					);
					error_dialog.response.connect (() => { error_dialog.hide (); });
					error_dialog.show ();
				}
			}
		});
		file_chooser.show ();
	}

	/**
	 * Creates a contact from the information in this window.
	 */
	public LrDB.Contact get_contact () {
		// Sets up the required classes
		if (contact == null) contact = new LrDB.Contact ();	// This ensures that custom X- and unimplemented properties remain
		ContactName name = new ContactName ();
		ContactDate birthdate = new ContactDate ();
		ContactAddress home_addr = new ContactAddress ();
		ContactAddress work_addr = new ContactAddress ();

		// Sets up the more advance names
		name.given = first_name_entry.text;
		name.family = last_name_entry.text;
		name.additional = middle_name_entry.text;
		name.prefixes = salutation_name_entry.text;
		contact.name = name;		 // Sets the Contact's name to the newly created name
		contact.full_name = name.prefixes + " " + name.given + " " + name.additional + " " + name.family;
		// Sets up the Birthdate
		if (birthday_switch.active) {
			birthdate.year = (uint) birthdate_year_spinbutton.get_value ();
			birthdate.month = (uint) birthdate_month_spinbutton.get_value ();
			birthdate.day = (uint) birthdate_day_spinbutton.get_value ();
			contact.birth_date = birthdate;
		}

		// Sets up PO Boxes
		if (work_address_text_view.buffer.text != "" ||
			work_postbox_entry.text != "" ||
			work_city_entry.text != "" ||
			work_county_entry.text != "" ||
			work_country_entry.text != "" ||
			work_zip_entry.text != ""
		) {
			work_addr.po = work_postbox_entry.text;
			work_addr.code = work_zip_entry.text;
			work_addr.country = work_country_entry.text;
			work_addr.region = work_county_entry.text;
			work_addr.locality = work_city_entry.text;
			work_addr.ext = work_address_text_view.buffer.text;
			contact.address_work = work_addr;
		}
		if (home_address_text_view.buffer.text != "" ||
			home_postbox_entry.text != "" ||
			home_city_entry.text != "" ||
			home_county_entry.text != "" ||
			home_country_entry.text != "" ||
			home_zip_entry.text != ""
		) {
			home_addr.po = home_postbox_entry.text;
			home_addr.code = home_zip_entry.text;
			home_addr.country = home_country_entry.text;
			home_addr.region = home_county_entry.text;
			home_addr.locality = home_city_entry.text;
			home_addr.ext = home_address_text_view.buffer.text;
			contact.address_home = home_addr;
		}

		// Sets up some work informaion
		if (job_role_entry.text != "") contact.role = job_role_entry.text;
		if (unit_entry.text != "") contact.org_unit = unit_entry.text;
		if (sub_unit_entry.text != "") contact.office = sub_unit_entry.text;
		if (organisation_entry.text != "") contact.org = organisation_entry.text;
		if (job_title_entry.text != "") contact.title = job_title_entry.text;

		// Sets up contactables
		contact.remove_all_contact_information();
		Widget widget = contact_listbox.get_first_child ();
		while (widget != null) {
			((ContactableEditor) ((Gtk.ListBoxRow)widget).child).update_to_contact(contact);
			widget = widget.get_next_sibling ();
		}
		// ¡Terminamos!
		return contact;
	}
}
