/*
 * lrdb_contact_information.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gee, GLib;

namespace LrDB {
	/// A string[] to convert between strings and ContactInformationPlatform
	public const string[] contact_information_platform_string = {
		"EMAIL",
		"TEL",
		"X-DISCORD",
		"X-MATRIX",
		"X-SKYPE",
		"X-AIM",
		"X-JABBER",
		"X-ICQ",
		"X-GROUPWISE",
		"X-GADUGADU"
	};
	/// An enum for different types of contact information
	public enum ContactInformationPlatform {
		EMAIL,
		TEL,
		DISCORD,
		MATRIX,
		SKYPE,
		AIM,
		JABBER,
		ICQ,
		GROUPWISE,
		GADUGADU
	}
	/// What is the type of a ContactInformation
	public enum ContactInformationType {
		PERSONAL,
		WORK,
		/// Only for TEL
		MOBILE
	}
	/// A string[] to convert the type of a ContactInformationType
	public const string[] contact_information_type_string = {
		"home",
		"work",
		"mobile"
	};
	public class ContactInformation : GLib.Object {
		/// The type of contact information.
		public ContactInformationPlatform platform {get; set; default=0;}
		/// The string containing the contact information.
		public string contact_string {get; set; default="";}
		/// How preferable this option is.
		public int preferability {get; set; default=0;}
		/// The uuid of this
		public string uuid;
		/// How public this is.
		public ContactInformationType contact_type {get; set; default=0;}
		/// Construct method
		construct {
			if (uuid==null)
				uuid = GLib.Uuid.string_random();
		}
		/// Creates a Contact Information from an Attribute
		public ContactInformation.from_attribute (E.VCardAttribute attr) {
			this.contact_string = attr.get_value();
			// Finds the platform
			int i = 0;
			foreach (string s in contact_information_platform_string) {
				if (s == attr.get_name()) {
					this.platform = (ContactInformationPlatform) i;
					break;
				}
				i++;
			}
			unowned var pref = attr.get_param("PREF");
			if (pref!=null)
				this.preferability = int.parse(pref.data);

			unowned var type = attr.get_param("TYPE");
			if (type!=null && type.length()!=0) {
				string type_val = type.data;
				if (type_val=="work")
					this.contact_type=WORK;
			}
			unowned var uuid = attr.get_param("PID");
			if (uuid!=null && uuid.length()!=0)
				this.uuid = uuid.data;
		}
		/// A method to add this to a E.VCard object
		public E.VCardAttribute give_vcard_attribute () {
			uuid = GLib.Uuid.string_random();
			// Creates an attribute
			var attribute = new E.VCardAttribute (null,
				contact_information_platform_string[platform]);

			var pref = new E.VCardAttributeParam ("PREF");
			print(@"$preferability");
			pref.add_value (preferability.to_string ());
			attribute.add_param (pref);

			var type = new E.VCardAttributeParam ("TYPE");
			type.add_value (contact_information_type_string[(int) this.contact_type]);

			var id = new E.VCardAttributeParam("UUID");
			id.add_value(uuid);
			
			attribute.add_param (type);
			attribute.add_value (contact_string);
			attribute.add_param (id);
			return attribute;
		}

		/// A method to check if a vcard property is for this to parse.
		public static bool can_parse_vcard_attribute (E.VCardAttribute attr) {
			return (attr.get_name () in contact_information_platform_string);
		}
	}
}
