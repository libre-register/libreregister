/*
 * lrdb_logon_user.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using GLib, Sqlite;
namespace LrDB {
	public class LogonUser : GLib.Object {
		public string username { get; set; default = "username"; }

		public string passwd { get; private set; }
		public string? unhashed_passwd;
		public string uuid;
		public int permissions;

		public void change_passwd (string passwd, int64 salt) {
			unhashed_passwd=passwd;
			string salt_computed = Checksum.compute_for_string (SHA512, salt.to_string () + Checksum.compute_for_string (SHA512, uuid));
			this.passwd = Checksum.compute_for_string (SHA512, passwd + salt_computed);
			for (int i = 0; i < 1000; i++) {
				this.passwd = Checksum.compute_for_string (SHA512, this.passwd);
			}
		}

		public bool validate_passwd (string passwd, int64 salt) {
			string salt_computed = Checksum.compute_for_string (SHA512, salt.to_string () + Checksum.compute_for_string (SHA512, uuid));
			string passwd_computed = Checksum.compute_for_string (SHA512, passwd + salt_computed);
			for (int i = 0; i < 1000; i++) {
				passwd_computed = Checksum.compute_for_string (SHA512, passwd_computed);
			}
			if (passwd_computed == this.passwd) return true;
			return false;
		}
	}
}
