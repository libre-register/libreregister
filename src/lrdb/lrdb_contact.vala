/*
 * lrdb_contact.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gee, GLib, LrDB;

namespace LrDB {
	// A more feature complete Contact with LrDB functionality.
	public class Contact : E.VCard {
		/// The Contact's Job Title.
		public string title {
			owned get {
				return get_vcard_sing_value ("TITLE");
			}
			set {
				set_vcard_sing_value ("TITLE", value);
			}
		}

		/// The Contact's Job Role.
		public string role {
			owned get {
				return get_vcard_sing_value ("ROLE");
			}
			set {
				set_vcard_sing_value ("ROLE", value);
			}
		}

		/// The Contact's Organisation
		public string org {
			owned get {
				return get_org_prop (0);
			}
			set {
				set_org_prop (value, 0);
			}
		}

		/// The Contact's Unit
		public string org_unit {
			owned get {
				return get_org_prop (1);
			}
			set {
				set_org_prop (value, 1);
			}
		}

		/// The Contact's Office/Sub Unit
		public string office {
			owned get {
				return get_org_prop (2);
			}
			set {
				set_org_prop (value, 2);
			}
		}

		/// The Contact's UUID
		public string id {
			owned get {
				return get_vcard_sing_value ("UID");
			}
			set {
				set_vcard_sing_value ("UID", value);
			}
		}

		/// The Contact's Birthdate
		public E.ContactDate? birth_date {
			owned get {
				var sing = get_vcard_sing_value ("BDAY");
				if (sing==null) {
					return null;
				}
				var date = E.ContactDate.from_string (sing);
				return date;
			} set {
				set_vcard_sing_value ("BDAY", value.to_string ());
			}
		}

		/// The Contact's Name
		public E.ContactName name {
			owned get {
				E.ContactName cn = new E.ContactName ();
				var attr = get_attribute ("N");
				{
					cn.family = "";
					cn.given = "";
					cn.additional = "";
					cn.prefixes = "";
					cn.suffixes="";
				}
				
				if (attr==null) {
					return cn;
				}
				
				unowned var list = attr.get_values ();
				var size = list.length ();
				for (int i = 0; i < size; i++) {
					var data = list.nth_data (i);
					switch (i) {
						case 0:
							cn.family = data;
							break;
						case 1:
							cn.given = data;
							break;
						case 2:
							cn.additional = data;
							break;
						case 3:
							cn.prefixes = data;
							break;
						case 4:
							cn.suffixes = data;
							break;
					}
				}
				return cn;
			} set {
				remove_attributes(null, "N");
				var attr = new E.VCardAttribute (null, "N");
				attr.add_value (value.family);
				attr.add_value (value.given);
				attr.add_value (value.additional);
				attr.add_value (value.prefixes);
				attr.add_value (value.suffixes);
				add_attribute (attr);
			}
		}

		/// The Contact's Home Address
		public E.ContactAddress address_home {
			owned get {
				return get_adr ("HOME");
			} set {
				set_adr ("HOME", value);
			}
		}
		/// The Contact's Work Address
		public E.ContactAddress address_work {
			owned get {
				return get_adr ("WORK");
			} set {
				set_adr ("WORK", value);
			}
		}
		/// The Contact's Formatted Full Name
		public string full_name {
			owned get {
				var v = get_vcard_sing_value ("FN");
				return v;
			} set {
				set_vcard_sing_value ("FN", value);
			}
		}
		construct {
			if (id==null)
				id = GLib.Uuid.string_random ();
		}
		
		public void add_contact_information (ContactInformation ci) {
			add_attribute(ci.give_vcard_attribute());
		}
		public string add_contact_information_direct(
			ContactInformationPlatform platform,
			ContactInformationType type,
			string str,
			int preferability=0) {
			ContactInformation ci = new ContactInformation();
			ci.contact_string=str;
			ci.platform=platform;
			ci.preferability=preferability;
			ci.contact_type=type;
			add_contact_information(ci);
			return ci.uuid;
		}
		/**
		 * Gets relavant contact information from this.
		 * @p service what service or null for all
		 */
		public Gee.ArrayList<ContactInformation> get_contact_information(ContactInformationPlatform? platform,
			ContactInformationType? type) {
			var list = new ArrayList<ContactInformation>();
			foreach (var attr in get_attributes())  {
				if (ContactInformation.can_parse_vcard_attribute(attr)) {
					var ci = new ContactInformation.from_attribute(attr);
					if ((platform==null || ci.platform==platform) &&
						(type==null || ci.contact_type==type)) {
						list.add(ci);
					}
				}
			}
			return list;
		}
		/**
		 * Removes a contact information based of PID
		 */
		public void remove_contact_information_by_uuid(string uuid) {
			foreach (var attr in get_attributes()) {
				if (ContactInformation.can_parse_vcard_attribute(attr)) {
					var ci = new ContactInformation.from_attribute(attr);
					if (ci.uuid==uuid)
						remove_attribute(attr);
				}
			}
		}
		/**
		 * Removes all the contact information
		 */
		public void remove_all_contact_information() {
			foreach (var attr in get_attributes()) {
				if (ContactInformation.can_parse_vcard_attribute(attr)) {
					remove_attribute(attr);
				}
			}
		}
		private void set_vcard_sing_value (string key, string val) {
			var attr = new E.VCardAttribute (null, key);
			attr.add_value (val);
			remove_attributes (null, key);
			add_attribute (attr);
		}

		// Gets a single faced vcar attribute's value
		private string? get_vcard_sing_value (string key) {
			var attr = get_attribute (key);
			if (attr!=null) return attr.get_value ();
			return "";
		}

		private E.VCardAttribute get_org_attr () {
			var attr = get_attribute ("ORG");
			if (attr == null) {
				attr = new E.VCardAttribute (null, "ORG");
				for (int i = 0; i < 3; i++) attr.add_value ("");
			}
			return attr;
		}

		private string get_org_prop (int n) {
			var attr = get_org_attr ();
			unowned var list = attr.get_values ();
			return list.nth_data (n);
		}

		private void set_org_prop (string s, int n) {
			var prev_attr = get_org_attr ();
			var attr = new E.VCardAttribute(null, "ORG");
			var list = prev_attr.get_values().copy();
			int i = 0;
			foreach (var val in list) {
				if (i == n)
					attr.add_value(s);
				else
					attr.add_value(val);
				i++;
			}
			remove_attributes("","ORG");
			add_attribute(attr);
			}

		// Gets and address
		private E.ContactAddress? get_adr (string s) {
			unowned var attrs = get_attributes ();
			// Loops through all the attributes
			for (int i = 0; i < attrs.length (); i++) {
				var attr = attrs.nth_data (i);
				unowned var params = attr.get_param ("TYPE");
				var applicable = false;

				if (attr.get_name ().up () != "ADR") // Checks if it is an address
					continue;
				if (attr.get_group () == s) // Checks if it is in the home group.
					applicable = true;
				if (!applicable && params != null && params.length () >= 1 && params.nth_data (0).up () == s) // Checks if it has a TYPE=HOME param;
					applicable = true;

				// If this is indeed the home address
				if (applicable) {
					var address = new E.ContactAddress ();
					unowned var values = attr.get_values ();
					// Populates the values.
					for (int j = 0; j < values.length (); j++) {
						string str = values.nth_data (j);
						switch (j) {
							case (0):
								address.po = str;
								break;
							case (1):
								address.ext = str;
								break;
							case (2):
								address.street = str;
								break;
							case (3):
								address.locality = str;
								break;
							case (4):
								address.region = str;
								break;
							case (5):
								address.code = str;
								break;
							case (6):
								address.country = str;
								break;
						}
					}
					return address;
				}
			}
			return null;
		}
		// Stes an address
		private void set_adr (string s, E.ContactAddress value) {
			unowned var prev_attrs  = get_attributes();
			// Removes any previous attributes
			foreach (var attr in prev_attrs) {
				if (attr.get_name()!="ADR")
					continue;
				unowned var param = attr.get_param("TYPE");
				if (attr.get_name()==s || (param.length() == 1 && param.data == s))
					remove_attribute(attr);
					continue;
			}
			if (value != null) {
				var attr = new E.VCardAttribute (s, "ADR");
				var param = new E.VCardAttributeParam ("TYPE");
				param.add_value (s);
				attr.add_value (value.po);
				attr.add_value (value.ext);
				attr.add_value (value.street);
				attr.add_value (value.locality);
				attr.add_value (value.region);
				attr.add_value (value.code);
				attr.add_value (value.country);
				attr.add_param (param);
				add_attribute(attr);
			} else {
				remove_attributes (s, "ADR");
			}
		}
	}
}
