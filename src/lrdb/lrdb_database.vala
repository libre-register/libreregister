/*
 * lrdb_database.vala
 *
 * Copyright 2020 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using Gee, GLib, Sqlite;

namespace LrDB {
	public class DataBase : GLib.Object {
		public uint8[]? image;
		public string name	{ get; set; }
		public string id{ get; set; }
		private ArrayList<LogonUser> logon_users;
		private ArrayList<LrDB.Contact> contacts;
		
		public signal void update_save_progress(double progress);
		public bool is_new {get; set; default=true;}
		public string? file {get; private set;}
		// Sql Statements
		private Gee.HashMap<string, string> sql_statements;
		
		public DataBase (string name, string id) {
			Object (name: name, id: id);
		}
		public DataBase.from_file(string file) {
			Object(file:file, is_new:true);
		}
		construct {
			logon_users = new Gee.ArrayList<LogonUser>();
			contacts = new Gee.ArrayList<LrDB.Contact>();
			sql_statements = new Gee.HashMap<string, string>();
		}
		public void setup_threaded_file_write(string s) {
			file=s;
		}
		public void write_to_new_file_threaded() {
			write_to_new_file(this.file);
		}
		/// Adds a contact to this.
		public void add_contact(LrDB.Contact contact) {
			// If nothing has been written to an sql database yet.
			if (is_new) {
				contacts.add(contact);
			}
			else {
				Sqlite.Database db;
				int ec = Sqlite.Database.open (file, out db);
				if (ec != Sqlite.OK) {
					critical (db.errmsg ());
				}
				add_contact_to_sql(db, contact, 0, 1);				
			}
		}
		/// Adds a logon user to this
		public void add_logon_user(LrDB.LogonUser logon_user) {
			if (is_new) {
				logon_users.add(logon_user);
			}
			else {
				Sqlite.Database db;
				int ec = Sqlite.Database.open (file, out db);
				if (ec != Sqlite.OK) {
					critical (db.errmsg ());
				}
			}
		}
		public void write_to_new_file (string path) {
			File file = File.parse_name(path);
			// For progress bars
			int max_queries = (
				6 + logon_users.size + contacts.size*24
			);
			int progress = 0;
			update_save_progress((double) progress/ (double) max_queries);
			Sqlite.Database db;
			int ec = Sqlite.Database.open (file.get_path(), out db);
			if (ec != Sqlite.OK) {
				critical (db.errmsg ());
			}
			// A query to create the tables.
			string query = get_sql_resource(
					"/org/libre_register/LrDB/setup_tables.sql"
				).printf (
					sql_string(name),
					sql_string(id)
				);
			// Run query
			run_query(db, query);
			progress+=6;
			update_save_progress((double) progress/ (double) max_queries);

			// Adds an image if the image is not null
			if (image != null) {
				run_query(db, 
					"""INSERT INTO Metadata
					(id, value) VALUES ('image',%s);""".printf (sql_string(Base64.encode (image))));
			}
			
			// Loops through logon users
			foreach (LogonUser logon_user in logon_users) {
				progress = add_logon_user_to_sql(db, logon_user, progress, max_queries);
			}
			// Contacts
			foreach (LrDB.Contact contact in contacts) {
				progress = add_contact_to_sql(db, contact, progress, max_queries);
			}
			update_save_progress(1);
		}
		// Adds a contact to an sql database.
		private int add_contact_to_sql(Sqlite.Database db, LrDB.Contact contact, int start_progress, double max_queries) {
			int progress = start_progress;
			// No null properties

			var home_address = contact.address_home;
			var work_address = contact.address_work;
			var name = contact.name;
			var birthdate = contact.birth_date;
			// Adds a contact with only a uuid
			run_query(db,
				get_sql_resource (
					"/org/libre_register/LrDB/add_contact.sql"
				).printf(sql_string(contact.id))
			);
			// Adds the contact's home_address
			if (home_address!=null) {
				run_query(db,
					get_sql_resource(
						"/org/libre_register/LrDB/update_home_address.sql"
					).printf (
						sql_string(home_address.po),
						sql_string(home_address.code),
						sql_string(home_address.country),
						sql_string(home_address.region),
						sql_string(home_address.locality),
						sql_string(home_address.ext),
						sql_string(contact.id)
					)
				);
			}
			progress+=6;
			update_save_progress((double) progress/ (double) max_queries);

			// Sets the contact's work address.
			if (work_address!=null) {
				run_query(db,
					get_sql_resource(
						"/org/libre_register/LrDB/update_work_address.sql"
					).printf (
						sql_string(work_address.po),
						sql_string(work_address.code),
						sql_string(work_address.country),
						sql_string(work_address.region),
						sql_string(work_address.locality),
						sql_string(work_address.ext),
						sql_string(contact.id)
					)
				);
			}
			progress+=6;
			update_save_progress((double) progress/ (double) max_queries);

			if (name!=null) {
				run_query(db,
					get_sql_resource(
						"/org/libre_register/LrDB/update_name.sql"
					).printf(
						sql_string(name.prefixes),
						sql_string(name.given),
						sql_string(name.additional),
						sql_string(name.family),
						sql_string(contact.id)
					)
				);
			}
			progress+=4;
			update_save_progress((double) progress/ (double) max_queries);

			// Contact Date
			if (birthdate!=null) {
				run_query (db,
					get_sql_resource(
						"/org/libre_register/LrDB/update_birthdate.sql"
					).printf(
						sql_string(EContactDate_to_string(birthdate)),
						sql_string(contact.id)

					)
				);
			}
			
			// Adds work information
			progress++;
			update_save_progress((double) progress/ (double) max_queries);
			run_query(db,
				get_sql_resource(
					"/org/libre_register/LrDB/update_org_information.sql"
				).printf (
					sql_string(contact.role),
					sql_string(contact.org_unit),
					sql_string(contact.office),
					sql_string(contact.org),
					sql_string(contact.title),
					sql_string(contact.id)
				)
			);
			progress+=5;
			update_save_progress((double) progress/ (double) max_queries);
			// Adds contact information
			foreach (var ci in contact.get_contact_information(null, null)) {
				var pref = true;
				if (ci.preferability>=1)
					pref = false;
				print("ContactType: %d \n",ci.contact_type);
				run_query(db,
					get_sql_resource(
						"/org/libre_register/LrDB/insert_contact_information.sql"
					).printf (
						sql_string(ci.uuid),
						sql_string(contact.id),
						sql_bool(pref),
						sql_string(ci.contact_string),
						sql_int(ci.platform),
						sql_int(ci.contact_type)
					)
				);
			}
			return progress;
		}
		private int add_logon_user_to_sql(Sqlite.Database db, LrDB.LogonUser logon_user, int progress = 0, double max_queries = 1) {
			run_query (db,
				get_sql_resource(
					"/org/libre_register/LrDB/insert_logon_user.sql"
				).printf (logon_user.username,
                	logon_user.passwd,
                	logon_user.uuid,
                	logon_user.permissions));
			update_save_progress((double) (progress+1)/ (double) max_queries);
	        return progress++;
		}
		private string? sql_string (string data) {
			if (data==null)
				return "NULL";
			return sql_variant(new Variant.string((string) data));
		}
		private string sql_int (int? data) {
			return data.to_string();
		}
		private string sql_bool (bool? data) {
			return data.to_string();
		}
		private string sql_double (double? data) {
			return data.to_string();
		}
		private string? sql_variant (Variant? vari) {
			if (vari == null) return "NULL";
			if (vari.get_data() == null ) return "NULL";
			switch (vari.classify ()) {
				// Bool
				case (BOOLEAN):
					if (vari.get_boolean ())
						return "1";
					return "0";

				// String, has anti SQL injection
				case (STRING):
					return prep_sqlite_string(vari.get_string());
				// Integers
				case (INT16):
					return vari.get_int16 ().to_string ();
				case (INT32):
					return vari.get_int32 ().to_string ();
				case (INT64):
					return vari.get_int64 ().to_string ();

				// Unsigned integers
				case (UINT16):
					return vari.get_uint16 ().to_string ();
				case (UINT32):
					return vari.get_uint32 ().to_string ();
				case (UINT64):
					return vari.get_uint64 ().to_string ();

				// Double
				case (DOUBLE):
					return vari.get_double ().to_string ();

				// Variant
				case (VARIANT):
					return sql_variant (vari.get_variant ());
				default:
					critical ("Type Not Supported");
					return null;
			}
		}

		private string EContactDate_to_string (E.ContactDate date) {
			return date.to_string () + " 00:00:00.000";
		}
		private string prep_sqlite_string(string s) {
			string ts = s;
			ts = ts.replace("\"", "\\\"");
			ts = ts.replace("'", "''");
			return ("\'"+ts+"\'");
		}
		private bool run_query(Sqlite.Database db, string query) {
			string errmsg;
			int ec = db.exec(query, null, out errmsg);
			if (ec != Sqlite.OK) {
				critical(errmsg);
				print(query);
				return false;
			}
			return true;
		}
		private string get_sql_resource(string path) {
			// Checks if already cached
			if (sql_statements.has_key(path))
				return sql_statements.get(path);
			// Otherwise add it to the cache
			try {
				var io_stream = GLib.resources_open_stream(path, NONE);
				var dis = new DataInputStream(io_stream);
				string line;
				var builder = new GLib.StringBuilder();
				while ((line = dis.read_line(null))!=null) {
					builder.append(line);
					builder.append("\n");
				}
				sql_statements.set(path, builder.str);
				return builder.str;
			} catch (Error e) {
				error(e.message);
			}
		}
	}
}
